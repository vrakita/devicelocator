<?php namespace DeviceLocator;

use DeviceLocator\Contracts\Locator;

class LocatorFactory {

	public static function find(Locator $driver, $parameters) {

		return $driver->locate($parameters);

	}

}