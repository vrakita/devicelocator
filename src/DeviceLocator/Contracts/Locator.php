<?php namespace DeviceLocator\Contracts;

interface Locator {

	/**
	 * Find longitude / latitude coordinates
	 * by given cell tower id
	 * 
	 * @param array $parameters
	 * @return array
	 * @throws \Exception
	 *
	 */
	public function locate($parameters);

}