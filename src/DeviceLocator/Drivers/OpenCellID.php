<?php namespace DeviceLocator\Drivers;

use DeviceLocator\Contracts\Locator;

class OpenCellID implements Locator {

	/**
	 * Find longitude / latitude coordinates
	 * by given cell tower id
	 * 
	 * @param array $parameters
	 * @return array
	 * @throws \Exception
	 *
	 */
	public function locate($parameters) {

		if( ! $response = @file_get_contents('http://opencellid.org/cell/get?key=a5f45b60-f3fc-4ab9-9f96-cf8909db203e&mcc=' . $parameters['mcc'] . '&mnc=' . $parameters['mnc'] . '&lac=' . $parameters['lac'] . 
			'&cellid=' . $parameters['cellid'] . '&format=json'))
			throw new \Exception("Error finding location in OpenCellID driver");


		$response = json_decode($response, true);

		if( ! isset($response['lon']) || ! isset($response['lat'])) throw new \Exception("Error parcing location coordinates in OpenCellID driver");


		return [
			'lat' => $response['lat'],
			'lon' => $response['lon'],
		];

		

	}

}